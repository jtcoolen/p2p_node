(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** {1 The routing table for a node} 
The nodes known to the current node instance, which are connected to it.
The table is designed to minimize the amount of hops for node lookups,
so that the amount of queried nodes is [log(network size)] on average.
*)

open Tezos_p2p

(** Node contact information. *)
module Node : sig
  module T : sig
    type t = Messages.node

    val sexp_of_t : t -> Sexplib0.Sexp.t

    val compare : t -> t -> int
  end

  include module type of T

  include module type of Base.Comparable.Make (T)
end

(** A node in Kademlia's routing table. *)
module Node_info : sig
  module T : sig
    type t = { node : Messages.node; first_seen : float; last_seen : float }

    val sexp_of_t : t -> Sexplib0.Sexp.t

    val compare : t -> t -> int
  end

  include module type of T

  include module type of Base.Comparable.Make (T)

  val encoding : t Data_encoding.t
end

(** The routing table. *)
module Table : sig
  type t

  val create : Node_id.t -> t

  val delete_node : t -> Messages.node -> unit

  val nodes : t -> Node_info.t list
  (** Returns all the nodes stored in the k-buckets.*)

  val store_node : net:('a, 'b, 'c) P2p.t -> Messages.node -> t -> unit Lwt.t

  val buckets : t -> int list

  val sorted_nodes : t -> Node_id.t -> Node_info.t list

  val closest_nodes' : t -> Node_id.t -> n:int -> Node_info.t list
  (** Returns the [n] closest nodes in [t.contents] with respect to [target]. *)

  val closest_nodes : t -> Node_id.t -> Node_info.t list
  (** Returns the [Constants.bucket_size] closest nodes in [t.contents] with respect to [target]. *)

  val bucket : t -> int -> Node_info.t list option

  val mem : t -> Node_id.t -> bool
end
