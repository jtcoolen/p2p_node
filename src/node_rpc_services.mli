(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_rpc

val rpc_directory : Proto.t -> unit RPC_directory.t
(** The set of RPC queries defined below that is passed to the
{{!val:Lib_dht__.Rpc.launch_rpc_server} [launch_rpc_server]} function. *)

(** {1 Type-safe RPC queries}
The RPC queries for interacting with the distributed hash table (DHT) or for 
node introspection.
*)

(** {2 Interacting with the DHT} *)

val store :
  rpc_addr:string ->
  rpc_port:int ->
  int32 ->
  int32 ->
  Node_id.t ->
  bytes ->
  string tzresult Lwt.t
(** [store ~rpc_addr ~rpc_port n total target data] asks to store a piece of
[data] at the specified RPC endpoint.
[n>=1] corresponds to the packet number and [total>=1] to the total number
of packets for the payload. [target] is the digest of the entire payload.
*)

val locate_value :
  rpc_addr:string ->
  rpc_port:int ->
  string ->
  (int32 * P2p_point.Id.t) tzresult Lwt.t
(** [locate_value ~rpc_addr ~rpc_port key] returns the size of the file with
digest [key] and the node to query to retrieve the file. *)

val get_chunk :
  rpc_addr:string ->
  rpc_port:int ->
  string ->
  P2p_point.Id.t ->
  int32 ->
  bytes tzresult Lwt.t
(** [get_chunk ~rpc_addr ~rpc_port key contact i] returns the chunk of the file whose
digest is [key], the node [contact] which stores the chunk and the index [i>=1]
of the chunk. *)

(** {2 Node introspection} *)
