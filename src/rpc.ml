(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Dynamic Ledger Solutions, Inc. <contact@tezos.com>     *)
(* Copyright (c) 2019-2021 Nomadic Labs, <contact@nomadic-labs.com>          *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_rpc
open Tezos_rpc_http
open Tezos_rpc_http_server

type error += RPC_Port_already_in_use of P2p_point.Id.t list

let () =
  register_error_kind `Permanent ~id:"node.rpc.port_already_in_use"
    ~title:"RPC port already in use" ~description:"RPC port already in use."
    ~pp:(fun ppf id ->
      Format.fprintf ppf "RPC port already in use: %s."
      @@ P2p_point.Id.to_string id)
    Data_encoding.(obj1 (req "p2p point" P2p_point.Id.encoding))
    (function RPC_Port_already_in_use [ p ] -> Some p | _ -> None)
    (fun p -> RPC_Port_already_in_use [ p ])

(* Add default accepted CORS headers *)
let sanitize_cors_headers ~default headers =
  List.map String.lowercase_ascii headers
  |> String.Set.of_list
  |> String.Set.(union (of_list default))
  |> String.Set.elements

let launch_rpc_server ~address:(host, port) ~rpc_dir =
  let build_rpc_directory =
    let dir : unit RPC_directory.t ref = ref RPC_directory.empty in
    let merge d = dir := RPC_directory.merge !dir d in
    let register0 s f =
      dir := RPC_directory.register !dir s (fun () p q -> f p q)
    in
    merge rpc_dir;
    register0 RPC_service.error_service (fun () () ->
        return (Data_encoding.Json.schema Error_monad.error_encoding));
    !dir
  in
  let cors_origins = [] in
  let cors_headers = [] in
  let dir =
    RPC_directory.register_describe_directory_service build_rpc_directory
      RPC_service.description_service
  in
  let mode = `TCP (`Port port) in
  let cors_headers =
    sanitize_cors_headers ~default:[ "Content-Type" ] cors_headers
  in
  Lwt.catch
    (fun () ->
      RPC_server.launch ~host mode dir ~media_types:Media_type.all_media_types
        ~cors:{ allowed_origins = cors_origins; allowed_headers = cors_headers }
      >>= return)
    (function
      | Unix.Unix_error (Unix.EADDRINUSE, "bind", "") ->
          fail (RPC_Port_already_in_use [ (P2p_addr.of_string_exn host, port) ])
      | exn -> Lwt.return (error_exn exn))
