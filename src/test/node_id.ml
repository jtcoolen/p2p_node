open Lib_dht__.Node_id

let n_id = QCheck.make @@ QCheck.Gen.return random_id

let u128 = QCheck.make @@ QCheck.Gen.int_bound 127

let xor_is_symetric =
  QCheck.Test.make ~count:1000 ~name:"xor_is_symetric"
    QCheck.(pair n_id n_id)
    (fun (a, b) ->
      let a, b = (a (), b ()) in
      Base.Int.(bucket_index a b = bucket_index b a))

let random_id_check =
  QCheck.Test.make ~count:1000 ~name:"random_id"
    QCheck.(pair n_id n_id)
    (fun (a, b) ->
      let a, b = (a (), b ()) in
      a <> b)

let random_id_within_distance =
  QCheck.Test.make ~count:1000 ~name:"random_id_within_distance"
    QCheck.(pair n_id u128)
    (fun (id, n) ->
      let id = id () in
      let rand_id = random_id_at_distance id n in
      Int.equal n (bucket_index rand_id id))

let suite =
  List.map QCheck_alcotest.to_alcotest
    [ xor_is_symetric; random_id_check; random_id_within_distance ]
