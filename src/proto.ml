(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_stdlib_unix
open Tezos_p2p
open Tezos_p2p_services
open Messages

type t = {
  net : (message, Peer_metadata.t, Connection_metadata.t) P2p.t;
  bootstrap : P2p_point.Id.t option;
  rpc_listening : P2p_point.Id.t;
  info : node;
  buckets : Routing.Table.t;
  store : (Node_id.t, string) Base.Hashtbl.t;
  pending_queries : (int64, Messages.message Lwt_condition.t) Base.Hashtbl.t;
  active_connections : (Node_id.t, int64) Base.Hashtbl.t;
  pending_files : (Node_id.t, int32) Base.Hashtbl.t;
  mutable data : bytes;
  mutable found_bootstrap : Routing.Node.t option;
  mutable msg_uuid : int64;
  mutable joined : bool;
}

type error += Value_not_found

let () =
  register_error_kind `Temporary ~id:"node.value_not_found"
    ~title:"Value not found" ~description:"Value node found."
    ~pp:(fun ppf () -> Format.fprintf ppf "Value node found.")
    Data_encoding.empty
    (function Value_not_found -> Some () | _ -> None)
    (fun () -> Value_not_found)

let increment_msg_uuid t =
  t.msg_uuid <-
    Int64.(match add t.msg_uuid 1L with i when i = max_int -> 0L | i -> i);
  t.msg_uuid

(** Sends the chunk of index [j] for the file with the given [digest] and [path]. *)
let send_store t ~conn path j ~uuid ~digest =
  let chan = open_in path in
  let size = Constants.block_size in
  let buffer = Bytes.create size in
  let rec loop ~eof i () =
    match not eof with
    | true -> (
        let len = input chan buffer 0 size in
        match (len > 0, i = j) with
        | true, true -> (
            P2p.send t.net conn
              (Get_value_response
                 (uuid, { key = digest; value = Bytes.sub buffer 0 len }))
            >>= function
            | _ -> loop ~eof:true i ())
        | true, false -> loop ~eof:false (Int32.succ i) ()
        | false, _ -> loop ~eof:true i ())
    | false ->
        close_in chan;
        Lwt_result.return ()
  in
  loop ~eof:false Int32.one ()

(* We extend the Set module with additional functions. *)
module Set1 = struct
  include Set

  module type S = sig
    include Set.S

    val take : t -> n:int -> t
    (** Returns the set of at most n smallest elements from the set. *)

    val to_list : t -> elt list
    (** Converts the set to a list of sorted elements. *)

    val take_as_list : t -> n:int -> elt list
    (** Returns at most n smallest sorted elements from the set as a list. *)

    val filter : f:(elt -> bool) -> t -> t

    val fold1 :
      f:('a -> elt -> ('b, 'c) result Lwt.t) ->
      g:('a -> 'b -> 'a) ->
      init:'a ->
      t ->
      ('a, 'c) Lwt_result.t

    val take_below : elt -> t -> t
    (** Returns the set of elements that are smaller than [threshold]. *)

    val exclude : without:t -> t -> t
    (** Returns the set of elements that aren't contained in the [without] set. *)
  end

  (** Builds a Set1 module from a given order relation. *)
  module Make1 (Ord : Stdlib.Set.OrderedType) : S with type elt = Ord.t = struct
    include Set.Make (Ord)

    let take t ~n =
      let rec aux n acc s =
        match (min_elt s, n) with
        | None, _ -> acc
        | Some _, 0 -> acc
        | Some min, _ -> aux (n - 1) (add min acc) (remove min s)
      in
      aux n empty t

    let to_list t = fold (fun e acc -> e :: acc) t []

    let take_as_list t ~n = take t ~n |> to_list

    let filter ~f = filter f

    let rec fold1 ~f ~g ~init s =
      match min_elt s with
      | None -> Lwt_result.return init
      | Some m ->
          f init m >>=? fun r -> fold1 ~f ~g ~init:(g init r) (remove m s)

    let take_below threshold = filter ~f:(( < ) threshold)

    let exclude ~without = filter ~f:(fun e -> not @@ mem e without)
  end
end

(** Returns a Set1 module with a custom order relation. *)
let make_set1 (type t) (compare : t -> t -> int) :
    (module Set1.S with type elt = t) =
  (module Set1.Make1 (struct
    type nonrec t = t

    let compare = compare
  end))

(** Returns a module for sets of nodes, ordered by their distance to [target]. *)
let make_node_set ~target =
  make_set1 (fun a b ->
      Node_id.compare (Node_id.xor a.id target) (Node_id.xor b.id target))

(** Appends the content of [value] to the appropriate file. *)
let store_data t ~key ~value =
  (* TODO:  In case of Duplicate, we might return an exception that
     is then passed to the REST API. *)
  let key = Node_id.to_b58check key in
  let f = Format.sprintf "./%s-%s" (Node_id.to_b58check t.info.id) key in
  let chan =
    if Sys.file_exists f then open_out_gen [ Open_append ] 777 f else open_out f
  in
  let (_ : unit) = output chan value 0 (Bytes.length value) in
  close_out chan

let wait_msg ~conn ~message_uuid ~timeout t =
  let open Base in
  let open Lwt.Infix in
  let cond = Lwt_condition.create () in
  let id = (P2p.connection_info t.net conn).peer_id in
  let (_ : [ `Duplicate | `Ok ]) =
    Hashtbl.add t.pending_queries ~key:message_uuid ~data:cond
  in

  Hashtbl.update t.active_connections id
    ~f:(Option.value_map ~default:Int64.one ~f:Int64.succ);

  let canceler = Lwt_canceler.create () in
  with_timeout ~canceler (Systime_os.sleep timeout) (fun _ ->
      Lwt_condition.wait cond >>= fun res -> Lwt_result.return res)

let locate_node t ~target =
  (* We first initialize a module for sets of nodes
     ordered by their distance to [target]. *)
  let module S = (val make_node_set ~target) in
  (* Ask [node] for [target], either get a set of [Constants.bucket_size]
     nodes closest to [target] or the [target] itself. *)
  let node_closest_contacts ~node ~target t =
    let message_uuid = increment_msg_uuid t in
    Network.connect_to_node ~net:t.net ~node >>= function
    | Some conn -> (
        P2p.send t.net conn (Find_node (message_uuid, target)) >>= fun _ ->
        wait_msg ~conn ~message_uuid
          ~timeout:(Misc.span_of_float_sec ~n_sec:30.)
          t
        >>=? function
        | Find_node_response (_, nodes) -> Lwt_result.return (S.of_list nodes)
        | _ -> Lwt_result.return S.empty)
    | None -> Lwt.return_ok S.empty
  in

  (* The lookup procedure.
     - shortlist: set of nodes to contact
     - contacts: set of [Constants.alpha] nodes to query at each iteration
     - probed: set of nodes that have already been queried so far

     At each iteration, we ask up to [Constant.alpha] nodes for their closest
     nodes.

     We accumulate these new closest nodes with [S.fold1].

     Then we update our shortlist to add the new nodes found and exclude the
     probed nodes.

     We stop once we've accumulated at least [Constants.bucket_size]
     closest nodes to [target]. We also stop when no new nodes found at an
     iteration are closer to the closest node to [target] found so far.
  *)
  (* TODO: contact nodes in parallel (don't wait for a node to answer
     in order to contact a new node). *)
  let rec locate_node' ~target ~shortlist ~probed ~closest_node t =
    let contacts = S.take shortlist ~n:Constants.alpha in
    let probed = S.union probed contacts in
    S.fold1
      ~f:(fun _ node -> node_closest_contacts ~node ~target t)
      ~g:S.union ~init:S.empty contacts
    >>=? fun new_nodes ->
    if S.cardinal probed >= Constants.bucket_size then
      Lwt_result.return (S.take_as_list probed ~n:Constants.bucket_size)
    else
      let shortlist = S.exclude (S.union shortlist new_nodes) ~without:probed in
      match closest_node with
      | Some min
        when (S.is_empty @@ S.take_below min new_nodes) && S.is_empty shortlist
        ->
          Lwt_result.return (S.to_list probed)
      | None ->
          locate_node' t ~target ~shortlist
            ~closest_node:(S.min_elt @@ S.union shortlist probed)
            ~probed
      | Some m ->
          locate_node' t ~target ~shortlist
            ~closest_node:(S.min_elt @@ S.add m (S.union shortlist probed))
            ~probed
  in
  locate_node' t ~target
    ~shortlist:
      (Routing.Table.nodes t.buckets
      |> Base.List.map ~f:(fun { node; _ } -> node)
      |> S.of_list)
    ~closest_node:None ~probed:S.empty

type 's locate_value_result =
  | Key_val of P2p_point.Id.t * int32
  | Closest of 's

let locate_value t ~target =
  (* The search is similar to locate_node, except that we return
     the first node which stores the value with digest [target]. *)
  let module S = (val make_node_set ~target) in
  let node_closest_contacts t ~node ~target =
    let message_uuid = increment_msg_uuid t in
    Network.connect_to_node ~net:t.net ~node >>= function
    | Some conn -> (
        P2p.send t.net conn (Find_value (message_uuid, target)) >>= fun _ ->
        wait_msg ~conn ~message_uuid
          ~timeout:(Misc.span_of_float_sec ~n_sec:30.)
          t
        >>=? function
        | Find_value_response (_, Value s) ->
            Lwt_result.return
            @@ Key_val ((node.ip_addr, Int32.to_int node.port), s)
        | Find_value_response (_, Closest_nodes n) ->
            Lwt_result.return @@ Closest (S.of_list n)
        | _ -> Lwt_result.return @@ Closest S.empty)
    | None -> Lwt_result.return @@ Closest S.empty
  in

  let rec locate_value' ~target ~shortlist ~probed ~closest_node t =
    (* Accumulates the results from the Find_value queries. Returns
       the first node which stores the value, if any. *)
    let rec loop t ~new_nodes ~target ~peers =
      match S.min_elt peers with
      | None -> Lwt_result.return @@ (Closest new_nodes, None)
      | Some node -> (
          node_closest_contacts ~node ~target t >>=? function
          | Key_val (point, s) ->
              Lwt_result.return @@ (Key_val (point, s), Some node)
          | Closest n ->
              loop t ~new_nodes:(S.union new_nodes n) ~target
                ~peers:(S.remove node peers))
    in
    let contacts = S.take shortlist ~n:Constants.alpha in
    let probed = S.union probed contacts in
    loop ~target ~new_nodes:S.empty ~peers:contacts t >>=? function
    | Key_val (point, packets), _ ->
        (* Returns the node to contact for later retrieval of the value,
           and the number of packets to query.*)
        Lwt_result.return (Key_val (point, packets))
    | Closest new_nodes, _ -> (
        let shortlist =
          S.exclude (S.union shortlist new_nodes) ~without:probed
        in
        match closest_node with
        | Some min
          when (S.is_empty @@ S.take_below min new_nodes)
               && S.is_empty shortlist ->
            Lwt_result.return @@ Closest (S.to_list probed)
        | None ->
            locate_value' t ~target ~shortlist
              ~closest_node:(S.min_elt @@ S.union shortlist probed)
              ~probed
        | Some m ->
            locate_value' t ~target ~shortlist
              ~closest_node:(S.min_elt @@ S.add m (S.union shortlist probed))
              ~probed)
  in
  locate_value' t ~target
    ~shortlist:
      (Routing.Table.nodes t.buckets
      |> Base.List.map ~f:(fun { node; _ } -> node)
      |> S.of_list)
    ~closest_node:None ~probed:S.empty
  >>=? function
  | Key_val (contact, packets) -> return (packets, contact)
  | Closest _ -> fail Value_not_found

let refresh_buckets t =
  Misc.iter
    ~f:(fun bucket _ ->
      locate_node t ~target:(Node_id.random_id_at_distance t.info.id bucket))
    (Routing.Table.buckets t.buckets)

type error += Bootstrap_failure

let join t ~bootstrap:(ip_addr, port) =
  let message_uuid = increment_msg_uuid t in
  (* Attemps to contact the bootstrapping node. *)
  (Network.connect ~net:t.net ~net_address:(ip_addr, port) >>= function
   | Some conn ->
       P2p.send t.net conn (Ping message_uuid) >>= fun _ ->
       wait_msg ~conn ~message_uuid
         ~timeout:(Misc.span_of_float_sec ~n_sec:30.)
         t
       >>= fun _ -> Lwt_result.return conn
   | None -> fail Bootstrap_failure)
  >>=? fun conn ->
  (* Inserts the gateway into the appropriate bucket in
     the routing table. *)
  Routing.Table.store_node ~net:t.net
    {
      id = (P2p.connection_info t.net conn).peer_id;
      ip_addr;
      port = Int32.of_int port;
    }
    t.buckets
  >>= fun _ ->
  (* Performs a node lookup to the current node ID. *)
  locate_node t ~target:t.info.id >>=? fun nodes ->
  (* Store the nodes found in the above node lookup. *)
  Misc.iter
    ~f:(fun n _ -> Routing.Table.store_node ~net:t.net n t.buckets)
    nodes
  >>=? fun () ->
  (* Refreshes all buckets (performs node lookups for node IDs in the
     range of each bucket, filling buckets that are further apart). *)
  refresh_buckets t >>= fun _ -> Lwt_result.return (t.joined <- true)

(* TODO: add a timeout there as well?  *)
let resolve ~id t ~message_uuid ~msg _ =
  let open Base.Hashtbl in
  match find t.pending_queries message_uuid with
  | None -> Lwt.return_unit
  | Some cond ->
      Lwt_condition.broadcast cond msg;
      remove t.pending_queries message_uuid;
      let open Int64 in
      update t.active_connections id ~f:(function
        | Some i when i <= zero -> zero
        | Some i when i > zero -> pred i
        | _ -> zero);
      (* Kill unused connections. *)
      iteri t.active_connections ~f:(fun ~key ~data ->
          if data = zero && t.joined && (not @@ Routing.Table.mem t.buckets key)
          then
            Misc.dont_wait
              ( (match P2p.find_connection_by_peer_id t.net key with
                | Some conn -> P2p.disconnect t.net conn
                | None -> Lwt.return ())
              >>= fun _ -> Lwt_result.return () ));
      Lwt.return_unit

let process_msg t ~conn ~msg ~sender ~id =
  let open Messages in
  let open Tezos_p2p in
  let open Lwt.Infix in
  let to_node ({ node; _ } : Routing.Node_info.t) = node in
  match msg with
  | Ping message_uuid ->
      (* Updates routing table and replies with an Ack. *)
      Misc.dont_wait
        ( Logging.Kademlia.(emit received_ping) sender.id >>= fun () ->
          Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
          P2p.send t.net conn (Ack message_uuid)
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Ack message_uuid ->
      (* TODO: We might want to deal with the potential result field in the Ack,
         ie. in case of I/O and lack of memory errors. *)
      Misc.dont_wait
        ( Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
          Logging.Kademlia.(emit received_ack) sender
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Store (message_uuid, n, total, { key; value }) ->
      (* Stores a key-value pair in its hash table. *)
      (* TODO: - Reply with an ACK? It may be useful to add a result field
          in the Ack to indicate the result of the action.
         - We might deal with the duplicate error as well. *)
      (* assert _n and _total are positive. *)
      Misc.dont_wait
        ( ( Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
            match n with
            | _ when Int32.(equal total n) ->
                let data =
                  Format.sprintf "./%s-%s"
                    (Node_id.to_b58check t.info.id)
                    (Node_id.to_b58check key)
                in
                let (_ : 'a) = Base.Hashtbl.add t.store ~key ~data in
                Base.Hashtbl.remove t.pending_files key;
                Lwt.return @@ store_data t ~key ~value
            | _ ->
                let (_ : 'a) = Base.Hashtbl.add t.pending_files ~key ~data:n in
                Lwt.return @@ store_data t ~key ~value )
        >>= fun () ->
          Logging.Kademlia.(emit store) key >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Find_node (message_uuid, node) ->
      (* Sends up to [bucket_size] triples [node] from the routing table
         which are closest to the key. *)
      Misc.dont_wait
        ( P2p.send t.net conn
            (Find_node_response
               ( message_uuid,
                 Base.List.map
                   (Routing.Table.closest_nodes t.buckets node)
                   ~f:to_node ))
        >>= fun _ ->
          Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
          Logging.Kademlia.(emit find_node) (node, sender)
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Find_node_response (message_uuid, _nodes) ->
      Misc.dont_wait
        ( Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
          Logging.Kademlia.(emit find_node_response) (_nodes, sender)
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Find_value (message_uuid, key) ->
      Misc.dont_wait
        ( (* Returns the amount of chunks if the requested value is stored in
             the current node instance, or the closest nodes to the [key]. *)
          (match Base.Hashtbl.find t.store key with
          | Some path ->
              let chan = open_in path in
              let size = Constants.block_size in
              let file_len = Int32.of_int @@ in_channel_length chan in
              let packets =
                Int32.(
                  add
                    (div file_len (of_int size))
                    (if not @@ equal (rem file_len (of_int size)) zero then one
                    else zero))
              in
              close_in chan;

              P2p.send t.net conn
              @@ Find_value_response (message_uuid, Value packets)
          | None ->
              let nodes =
                Base.List.map
                  (Routing.Table.closest_nodes t.buckets key)
                  ~f:to_node
              in
              P2p.send t.net conn
                (Find_value_response (message_uuid, Closest_nodes nodes)))
        >>= fun _ ->
          Logging.Kademlia.(emit find_value) (key, sender) >>= fun _ ->
          Routing.Table.store_node ~net:t.net sender t.buckets
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Find_value_response (message_uuid, find_value) ->
      Misc.dont_wait
        ( Routing.Table.store_node ~net:t.net sender t.buckets >>= fun _ ->
          Logging.Kademlia.(emit find_value_response) (find_value, sender)
          >>= resolve ~message_uuid ~msg ~id t
          >>= fun () -> Lwt_result.return () );
      Lwt.return_unit
  | Get_value (message_uuid, i, key) ->
      (* Replies back with the chunk of index i of the file
         with digest equal to [key]. *)
      Misc.dont_wait
        (let f =
           Format.sprintf "./%s-%s"
             (Node_id.to_b58check t.info.id)
             (Node_id.to_b58check key)
         in
         (match Base.Hashtbl.mem t.store key with
         | true -> send_store t ~conn f i ~uuid:message_uuid ~digest:key
         | false -> Lwt_result.return ())
         >>= fun _ ->
         Routing.Table.store_node ~net:t.net sender t.buckets
         >>= resolve ~message_uuid ~msg ~id t
         >>= fun () -> Lwt_result.return ());
      Lwt.return_unit
  | Get_value_response (message_uuid, _) ->
      Misc.dont_wait
        ( resolve ~message_uuid ~msg ~id t () >>= fun () ->
          Routing.Table.store_node ~net:t.net sender t.buckets >>= fun () ->
          Lwt_result.return () );
      Lwt.return_unit

type error += Locate

let store t n total target value =
  locate_node t ~target >>=? function
  | s -> (
      let message_uuid = increment_msg_uuid t in
      match s with
      | [] -> Error_monad.fail Locate
      | nodes ->
          let rec loop = function
            | [] -> Lwt_result.return (Node_id.to_b58check target)
            | node :: rest -> (
                Network.connect_to_node ~net:t.net ~node >>= function
                | Some conn ->
                    P2p.send t.net conn
                      (Store (message_uuid, n, total, { key = target; value }))
                    >>=? fun _ -> loop rest
                | None -> Lwt_result.return () >>=? fun _ -> loop rest)
          in
          loop nodes)

let get t key ip_addr port i =
  let target = Node_id.of_b58check_exn key in
  Format.eprintf "Contact: %s : %d@." (Ip_addr.to_string ip_addr) port;
  Format.eprintf "Key: %s@." key;
  if
    Ip_addr.compare t.info.ip_addr ip_addr = 0
    && t.info.port = Int32.of_int port
  then fail Value_not_found
  else
    Network.connect_res ~net:t.net ~net_address:(ip_addr, port) >>=? fun conn ->
    let message_uuid = increment_msg_uuid t in
    P2p.send t.net conn (Get_value (message_uuid, i, target)) >>= fun _ ->
    Format.eprintf "SENT! %s %d@." (ip_addr |> Ip_addr.to_string) port;
    wait_msg ~message_uuid ~conn ~timeout:(Misc.span_of_float_sec ~n_sec:30.) t
    >>=? function
    | Get_value_response (_, { value; _ }) -> return value
    | _ -> fail Value_not_found
