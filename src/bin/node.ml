(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives

let command =
  let open Core in
  Command.basic ~summary:"Launch a Kademlia peer."
    Command.Let_syntax.(
      let%map_open listening =
        flag "--listening" (optional string)
          ~doc:
            "addr:port Listening address and port of the peer, separated by a \
             `:`.\n\
             Defaults to :::10000 (listen to all IPs, ipv4 and ipv6)."
      and bootstrap =
        flag "--bootstrap" (optional string)
          ~doc:"addr:port Address of a peer to join in the network."
      and rpc =
        flag "--rpc" (optional string)
          ~doc:
            "addr:port RPC server host address and port, separated by a `:`.\n\
             Defaults to ::ffff:127.0.0.1:30000."
      and _verbose = flag "-v" no_arg ~doc:"Verbose mode." in
      fun () ->
        let listening =
          Misc.parse_addr listening
          |> Option.value
               ~default:(Lib_dht.Net_address.make ~ip:"[::]" ~port:10000)
        in
        let bootstrap = Misc.parse_addr bootstrap in
        let rpc_listening =
          Misc.parse_addr rpc
          |> Option.value
               ~default:
                 (Lib_dht.Net_address.make ~ip:"::ffff:127.0.0.1" ~port:30000)
        in
        match
          Lwt_main.run
            (Lib_dht.init ~listening ~bootstrap ~rpc_listening >>=? Lib_dht.run)
        with
        | Error trace ->
            Format.eprintf "Error: %a\n%!" Error_monad.pp_print_error_first
              trace
        | Ok () -> ())

let () = Core.Command.run command
