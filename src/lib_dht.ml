(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2021 Nomadic Labs <contact@nomadic-labs.com>                *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Tezos_base.TzPervasives
open Tezos_p2p
open Network
open Messages
open Rpc
open Proto
open Routing

module Node_id = struct
  include Node_id

  let digest path =
    let chan = open_in path in
    let size = Constants.block_size in
    let buffer = Bytes.create size in

    let rec loop ~eof ~hashes () =
      match not eof with
      | true -> (
          let len = input chan buffer 0 size in
          match Base.Int.(len > 0) with
          | true ->
              let h =
                [ Bytes.sub buffer 0 len ] |> Node_id.hash_bytes
                |> Node_id.to_bytes
              in
              loop ~eof:false ~hashes:(h :: hashes) ()
          | false -> loop ~eof:true ~hashes ())
      | false -> Node_id.hash_bytes hashes
    in
    loop ~eof:false ~hashes:[] ()
end

module Net_address = struct
  include P2p_point.Id

  let make ~ip ~port = (P2p_addr.of_string_exn ip, port)
end

type t = Proto.t

(* System limits used by the maintenance and welcome worker. *)
let default_p2p_limits : P2p.limits =
  let greylist_timeout = Time.System.Span.of_seconds_exn 86400. (* one day *) in
  {
    connection_timeout = Time.System.Span.of_seconds_exn 10.;
    authentication_timeout = Time.System.Span.of_seconds_exn 5.;
    greylist_timeout;
    maintenance_idle_time =
      Time.System.Span.of_seconds_exn 120. (* two minutes *);
    min_connections = 40;
    expected_connections = 50;
    max_connections = 500;
    backlog = 20;
    max_incoming_connections = 600;
    max_download_speed = None;
    max_upload_speed = None;
    read_buffer_size = 1 lsl 14;
    read_queue_size = None;
    write_queue_size = None;
    incoming_app_message_queue_size = None;
    incoming_message_queue_size = None;
    outgoing_message_queue_size = None;
    max_known_points = Some (400, 300);
    max_known_peer_ids = Some (400, 300);
    peer_greylist_size = 1023 (* historical value *);
    ip_greylist_size_in_kilobytes =
      2 * 1024 (* two megabytes has shown good properties in simulation *);
    ip_greylist_cleanup_delay = greylist_timeout;
    swap_linger = Time.System.Span.of_seconds_exn 30.;
    binary_chunks_size = None;
  }

(* Network configuration. *)
let default_p2p_config ?(discovery_port = None) ~trusted_points listening_addr
    listening_port : P2p.config =
  {
    listening_port = Some listening_port;
    listening_addr = Some listening_addr;
    (* Trusted points peers can bootstrap from. *)
    trusted_points;
    identity = P2p_identity.generate_with_pow_target_0 ();
    (* Proof of work to prevent DoS from happening. *)
    proof_of_work_target = Tezos_crypto.Crypto_box.make_pow_target 0.;
    trust_discovered_peers = true;
    discovery_addr = Option.map (fun _ -> Ipaddr.V4.localhost) discovery_port;
    discovery_port;
    private_mode = false;
    peers_file = "/dev/null";
    reconnection_config = P2p_point_state.Info.default_reconnection_config;
    too_many_connections = None;
  }

let init ~listening:(addr, port) ~bootstrap ~rpc_listening =
  let trusted_points =
    match bootstrap with None -> [] | Some b -> [ (b, None) ]
  in
  let config = default_p2p_config addr port ~trusted_points in
  init_node ~config ~limits:default_p2p_limits ~msg_config >>=? fun net ->
  let id = P2p.peer_id net in
  Lwt_result.return
    {
      net;
      bootstrap;
      rpc_listening;
      info = { id; ip_addr = Ipaddr.V6.localhost; port = Int32.of_int port };
      buckets = Table.create id;
      store = Base.Hashtbl.create (module Node_id);
      pending_queries = Base.Hashtbl.create (module Base.Int64);
      active_connections = Base.Hashtbl.create (module Node_id);
      pending_files = Base.Hashtbl.create (module Node_id);
      data = Base.Bytes.init 1 ~f:(fun _ -> '0');
      found_bootstrap = None;
      msg_uuid = 0L;
      joined = false;
    }

let handle_new_connections t =
  P2p.on_new_connection t.net (fun id' conn ->
      let peer_info = P2p.connection_info t.net conn in
      let id = peer_info.peer_id in
      assert (id = id');
      let ip_addr, port = peer_info.id_point in
      (* Main loop, receiving and processing incoming messages for the
         current connection. *)
      let rec loop ~port () =
        let sender = { id; ip_addr; port = Int32.of_int port } in
        P2p.recv t.net conn >>=? fun msg ->
        process_msg t ~conn ~msg ~sender ~id >>= fun _ -> loop ~port ()
      in
      match (port, t.bootstrap) with
      | None, _ -> (* If no port is supplied, abort. *) ()
      | Some port, Some bootstrap
        when Ip_addr.compare (fst bootstrap) ip_addr = 0
             && Int.compare (snd bootstrap) port = 0
             && not t.joined ->
          (* If the node hasn't joined the network yet
             (filled its routing table). *)
          Misc.dont_wait
            ( join t ~bootstrap >>= fun _ ->
              Logging.Kademlia.(emit join) () |> Lwt_result.ok );
          Misc.dont_wait @@ loop ~port ()
      | Some port, _ -> Misc.dont_wait @@ loop ~port ())

let run t =
  Tezos_stdlib_unix.Internal_event_unix.init ()
  (* Activate logging system. *) >>= fun () ->
  (* Initialize a peer using the configuration and system limits records. *)
  launch_rpc_server
    ~address:(P2p_addr.to_string (fst t.rpc_listening), snd t.rpc_listening)
    ~rpc_dir:(Node_rpc_services.rpc_directory t)
  >>=? fun rpc ->
  let (_ : Lwt_exit.clean_up_callback_id) =
    Lwt_exit.register_clean_up_callback ~loc:__LOC__ ~after:[] (fun _ ->
        Logging.Rpc.(emit shutting_down_rpc_server) () >>= fun () ->
        List.iter_p Tezos_rpc_http_server.RPC_server.shutdown [ rpc ])
  in
  Lwt_result.return () >>= fun _ ->
  (* Execute the following program for incoming connections. *)
  handle_new_connections t;

  (* Activate peer: launch maintenance and welcome worker.
     The latter being in charge of accepting incoming connections
     and adding them to the pool. *)
  P2p.activate t.net;

  Logging.Kademlia.(emit show_peer_id) t.info.id >>= fun () ->
  (* Bootstrap, if not already done. *)
  (if Option.is_some t.bootstrap && not t.joined then
   join t ~bootstrap:(Base.Option.value_exn t.bootstrap)
  else Lwt_result.return ())
  >>= fun _ -> Lwt_utils.never_ending ()

let store_generic path f =
  let digest = Node_id.digest path in
  let chan = open_in path in
  let size = Constants.block_size (* 4 * 1024 *) in
  let file_len = Int32.of_int @@ in_channel_length chan in
  let open Int32 in
  let packets_amount =
    add
      (div file_len (of_int size))
      (if not @@ equal (rem file_len (of_int size)) zero then one else zero)
  in
  let buffer = Bytes.create size in

  let rec loop ~eof i =
    match not eof with
    | true -> (
        let len = input chan buffer 0 size in
        match len > 0 with
        | true ->
            f (succ i) packets_amount digest (Bytes.sub buffer 0 len)
            >>=? fun _ -> loop ~eof:false (succ i)
        | false -> loop ~eof:true i)
    | false ->
        (*assert (equal i packets_amount);*)
        Lwt_result.return digest
  in
  loop ~eof:false Int32.zero

let store ~rpc_host:(rpc_addr, rpc_port) path =
  store_generic path
    (Node_rpc_services.store ~rpc_addr:(P2p_addr.to_string rpc_addr) ~rpc_port)

let store_local t path = store_generic path (Proto.store t)

let get_generic locate get ~key ~dest =
  locate key >>=? fun (packets_amount, contact) ->
  let f = Format.sprintf "%s" dest in
  let chan = Caml.open_out f in

  let rec loop i =
    match i with
    | i when Int32.(equal i packets_amount) ->
        Lwt_result.return @@ Caml.close_out chan
    | _ ->
        get key contact (Int32.succ i) >>=? fun value ->
        let len = Bytes.length value in
        Caml.output chan value 0 len;
        loop (Int32.succ i)
  in
  loop Int32.zero

let get ~rpc_host:(rpc_addr, rpc_port) ~key ~destination =
  get_generic
    (Node_rpc_services.locate_value
       ~rpc_addr:(P2p_addr.to_string rpc_addr)
       ~rpc_port)
    (Node_rpc_services.get_chunk
       ~rpc_addr:(P2p_addr.to_string rpc_addr)
       ~rpc_port)
    ~key ~dest:destination

(* TODO: get rid of RPC host address. *)
let get_local ~rpc_host:(rpc_addr, rpc_port) t ~key ~destination =
  get_generic
    (Node_rpc_services.locate_value
       ~rpc_addr:(P2p_addr.to_string rpc_addr)
       ~rpc_port)
    (fun key (ip, port) i -> Proto.get t key ip port i)
    ~key ~dest:destination
