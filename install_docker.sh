#! /bin/sh

sudo chmod -R 777 .git .
git submodule init
git submodule update
cd tezos
opam install --yes --unlock-base ocaml-base-compiler.4.12.0
eval $(opam env)
sudo apt install -y apt-utils
sudo apt install -y rsync git m4 build-essential patch unzip wget pkg-config libgmp-dev libev-dev libhidapi-dev libffi-dev opam jq zlib1g-dev libssl-dev sqlite3
wget https://sh.rustup.rs/rustup-init.sh
chmod +x rustup-init.sh
./rustup-init.sh --profile minimal --default-toolchain 1.44.0 -y
source $HOME/.cargo/env
opam init --bare -n --bypass-checks
make build-deps
eval $(opam env)
make
opam update
sh ./scripts/opam-pin.sh
opam repo add opam https://opam.ocaml.org
opam update
cd ..
eval $(opam env --switch=$(pwd)/tezos --set-switch)
opam install --yes merlin odoc utop ocamlformat ocaml-lsp-server --destdir /src/opam-install
opam install . -y --deps-only --destdir /src/opam-install
opam install tezos/tezt/lib/tezt.opam --destdir /src/opam-install
ln tezos/_opam/ _opam -s
