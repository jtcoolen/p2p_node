#! /bin/sh

git submodule init
git submodule update
cd tezos
opam install --yes --unlock-base ocaml-base-compiler.4.12.0
eval $(opam env)
opam init --bare -n --bypass-checks
make build-deps
#opam pin data-encoding /home/julien/data-encoding --yes
#opam update --yes
eval $(opam env)
make
opam update
sh ./scripts/opam-pin.sh
opam repo add opam https://opam.ocaml.org
opam update

cd ..
eval $(opam env --switch=$(pwd)/tezos --set-switch)
opam install --yes merlin odoc utop ocamlformat ocaml-lsp-server
opam install . -y --deps-only
opam install tezos/tezt/lib/tezt.opam
ln tezos/_opam/ _opam -s
