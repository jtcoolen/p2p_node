(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020-2021 Nomadic Labs <contact@nomadic-labs.com>           *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type argument = Port of int | Bootstrap of string

let make_argument = function
  | Port x -> [ "--port"; string_of_int x ]
  | Bootstrap x -> [ "--bootstrap"; x ]

let make_arguments arguments = List.flatten (List.map make_argument arguments)

type 'a known = Unknown | Known of 'a

module Parameters = struct
  type persistent_state = {
    mutable net_addr : string;
    mutable rpc_addr : string option;
    mutable bootstrap_addr : string option;
    default_expected_pow : int;
    mutable arguments : argument list;
    mutable pending_ready : unit option Lwt.u list;
    mutable pending_level : (int * int option Lwt.u) list;
    mutable pending_identity : string option Lwt.u list;
    runner : Runner.t option;
  }

  type session_state = {
    mutable ready : bool;
    mutable level : int known;
    mutable identity : string known;
  }

  let base_default_name = "node"

  let default_colors = Log.Color.[| FG.cyan; FG.magenta; FG.yellow; FG.green |]
end

open Parameters
include Daemon.Make (Parameters)

let check_error ?exit_code ?msg node =
  match node.status with
  | Not_running ->
      Test.fail "node %s is not running, it has no stderr" (name node)
  | Running { process; _ } -> Process.check_error ?exit_code ?msg process

let wait node =
  match node.status with
  | Not_running ->
      Test.fail "node %s is not running, cannot wait for it to terminate"
        (name node)
  | Running { process; _ } -> Process.wait process

let name node = node.name

let runner node = node.persistent_state.runner

let next_port = ref Cli.options.starting_port

let fresh_port () =
  let port = !next_port in
  incr next_port;
  port

let () =
  Test.declare_reset_function @@ fun () ->
  next_port := Cli.options.starting_port

let spawn_command node =
  Process.spawn ?runner:node.persistent_state.runner ~name:node.name
    ~color:node.color node.path

let trigger_ready node value =
  let pending = node.persistent_state.pending_ready in
  node.persistent_state.pending_ready <- [];
  List.iter (fun pending -> Lwt.wakeup_later pending value) pending

let set_ready node =
  (match node.status with
  | Not_running -> ()
  | Running status -> status.session_state.ready <- true);
  trigger_ready node (Some ())

let update_level node current_level =
  (match node.status with
  | Not_running -> ()
  | Running status -> (
      match status.session_state.level with
      | Unknown -> status.session_state.level <- Known current_level
      | Known old_level ->
          status.session_state.level <- Known (max old_level current_level)));
  let pending = node.persistent_state.pending_level in
  node.persistent_state.pending_level <- [];
  List.iter
    (fun ((level, resolver) as pending) ->
      if current_level >= level then
        Lwt.wakeup_later resolver (Some current_level)
      else
        node.persistent_state.pending_level <-
          pending :: node.persistent_state.pending_level)
    pending

let update_identity node identity =
  match node.status with
  | Not_running -> ()
  | Running status ->
      (match status.session_state.identity with
      | Unknown -> status.session_state.identity <- Known identity
      | Known identity' ->
          if identity' <> identity then Test.fail "node identity changed");
      let pending = node.persistent_state.pending_identity in
      node.persistent_state.pending_identity <- [];
      List.iter
        (fun resolver -> Lwt.wakeup_later resolver (Some identity))
        pending

let handle_event node { name; value } =
  match name with
  | "node_is_ready.v0" -> set_ready node
  | "node_chain_validator.v0" -> (
      match JSON.as_list_opt value with
      | Some [ _timestamp; details ] -> (
          match JSON.(details |-> "event" |-> "level" |> as_int_opt) with
          | None ->
              (* There are several kinds of [node_chain_validator.v0] events
                 and maybe this one is not the one with the level: ignore it. *)
              ()
          | Some level -> update_level node level)
      | _ ->
          (* Other kind of node_chain_validator event that we don't care about. *)
          ())
  | "read_identity.v0" -> update_identity node (JSON.as_string value)
  | _ -> ()

let check_event ?where node name promise =
  let* result = promise in
  match result with
  | None ->
      raise
        (Terminated_before_event { daemon = node.name; event = name; where })
  | Some x -> return x

let wait_for_ready node =
  match node.status with
  | Running { session_state = { ready = true; _ }; _ } -> unit
  | Not_running | Running { session_state = { ready = false; _ }; _ } ->
      let promise, resolver = Lwt.task () in
      node.persistent_state.pending_ready <-
        resolver :: node.persistent_state.pending_ready;
      check_event node "node_is_ready.v0" promise

let wait_for_level node level =
  match node.status with
  | Running { session_state = { level = Known current_level; _ }; _ }
    when current_level >= level ->
      return current_level
  | Not_running | Running _ ->
      let promise, resolver = Lwt.task () in
      node.persistent_state.pending_level <-
        (level, resolver) :: node.persistent_state.pending_level;
      check_event node "node_chain_validator.v0"
        ~where:("level >= " ^ string_of_int level)
        promise

let wait_for_identity node =
  match node.status with
  | Running { session_state = { identity = Known identity; _ }; _ } ->
      return identity
  | Not_running | Running _ ->
      let promise, resolver = Lwt.task () in
      node.persistent_state.pending_identity <-
        resolver :: node.persistent_state.pending_identity;
      check_event node "read_identity.v0" promise

let create2 ?runner ~name ?color ?event_pipe ~net_addr ~rpc_addr ~bootstrap_addr
    () =
  let node =
    create ?runner ~path:"./node" ~name ?color
      ?event_pipe
      {
        net_addr;
        rpc_addr;
        bootstrap_addr;
        arguments = [];
        default_expected_pow = 0;
        runner;
        pending_ready = [];
        pending_level = [];
        pending_identity = [];
      }
  in
  on_event node (handle_event node);
  node

let add_argument node argument =
  node.persistent_state.arguments <- argument :: node.persistent_state.arguments

let run ?(on_terminate = fun _ -> ()) ?event_level node arguments =
  (match node.status with
  | Not_running -> ()
  | Running _ -> Test.fail "node %s is already running" node.name);
  let event_level =
    match event_level with
    | Some level -> (
        match String.lowercase_ascii level with
        | "debug" | "info" | "notice" -> event_level
        | _ ->
            Log.warn
              "Node.run: Invalid argument event_level:%s. Possible values are: \
               debug, info, and notice. Keeping default level (notice)."
              level;
            None)
    | None -> None
  in
  let arguments = node.persistent_state.arguments @ arguments in
  let arguments =
    List.concat
      [
        [ "--listening"; node.persistent_state.net_addr ];
        (match node.persistent_state.rpc_addr with
        | None -> []
        | Some s -> [ "--rpc"; s ]);
        (match node.persistent_state.bootstrap_addr with
        | None -> []
        | Some s -> [ "--bootstrap"; s ]);
        make_arguments arguments;
      ]
  in
  let on_terminate status =
    on_terminate status;
    (* Cancel all [Ready] event listeners. *)
    trigger_ready node None;
    (* Cancel all [Level_at_least] event listeners. *)
    let pending = node.persistent_state.pending_level in
    node.persistent_state.pending_level <- [];
    List.iter (fun (_, pending) -> Lwt.wakeup_later pending None) pending;
    (* Cancel all [Read_identity] event listeners. *)
    let pending = node.persistent_state.pending_identity in
    node.persistent_state.pending_identity <- [];
    List.iter (fun pending -> Lwt.wakeup_later pending None) pending;
    unit
  in
  run ?runner:node.persistent_state.runner ?event_level node
    { ready = false; level = Unknown; identity = Unknown }
    arguments ~on_terminate

let init ?runner ~name ?color ?event_pipe ~net_addr ?(rpc_addr = None)
    ?(bootstrap_addr = None) ?event_level () =
  let node =
    create2 ?runner ~name ?color ?event_pipe ~net_addr ~rpc_addr ~bootstrap_addr
      ()
  in
  let* () = run ?event_level node [] in
  (*let* () = wait_for_ready node in*)
  return node

let restart node arguments =
  let* () = terminate node in
  let* () = run node arguments in
  wait_for_ready node
